<?php
namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use common\models\OmsArticle;

class AritcleController extends Controller
{
    public function actionIndex()
    {
        $id = isset($_GET['id'])?trim($_GET['id']):'';
        $article = \Yii::$app->cache->get('aritcleIndex'.date("Ymd").$id);
        if (empty($article)){
            $article = OmsArticle::find()->where(['id'=>$id])->one();
            \Yii::$app->cache->set('aritcleIndex'.date("Ymd").$id, $article,3600*1);
        }
        return $this->render('index',[
            'article' => $article,
        ]);
    }

}
