<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "{{%sys_category}}".
 *
 * @property string $id
 * @property string $type_name
 * @property integer $parent_id
 * @property integer $sort
 */
class SysCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sys_category}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_name'], 'required'],
            [['parent_id', 'sort'], 'integer'],
            [['type_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type_name' => '分类名',
            'parent_id' => '父类ID',
            'sort' => '排序',
        ];
    }
}
