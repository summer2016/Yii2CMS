<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\component\Ueditor;

/* @var $this yii\web\View */
/* @var $model common\models\OmsArticle */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
<!--

-->
</style>
<div class="oms-article-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?php //$form->field($model, 'content')->textarea(['rows' => 6]); ?>

    <div style="width: 100%;height:700px;margin-bottom:10px;">
    <?php
    echo Ueditor::widget([
        'name'=>'content',
        'options'=>[
            'id'=>'txtContent',
            'focus'=>true,
            'toolbars'=> [
                [
        			'fullscreen', 'source', 'undo', 'redo', '|',
        			'fontsize',
        			'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat',
        			'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|',
        			'forecolor', 'backcolor', '|',
        			'lineheight', '|',
        			'indent', '|',
        			'link', '|',
        			'simpleupload','|','insertimage', 'insertvideo'
    		    ],
            ],
        ],
        'attributes'=>[
            'style'=>'height:400px;width:100%;'
        ]
    ]);
    ?>
    </div>
    <?php 
        //设置表单中作者的值
        if ($model->author){
            $value = $model->author;
        }else {
            $value = \Yii::$app->user->identity->login_account;
        }
    ?>
    <?= $form->field($model, 'add_time')->textInput(['value'=>$model->add_time,'readonly'=>true]) ?>

    <?= $form->field($model, 'author')->textInput(['maxlength' => 255,'value'=>$value,'readonly'=>true]) ?>

    <?php //$form->field($model, 'type_id')->textInput(); ?>
    <?php if (!empty($cateGory)){?>
    <div class="form-group field-omsarticle-type_id">
        <label for="omsarticle-type_id" class="control-label">分类：</label>
        <select name='OmsArticle[type_id]' id="omsarticle-type_id">
            <?php foreach ($cateGory as $v){?>
            <option value="<?=$v['id'] ?>"><?=$v['type_name'] ?></option>
            <?php }?>
        </select>
        <div class="help-block"></div>
    </div>
    <?php }?>
    <?php //$form->field($model, 'status')->textInput() ?>
    <div class="form-group field-omsarticle-status">
        <label for="omsarticle-status" class="control-label">状态：</label>
        <select name='OmsArticle[status]' id="omsarticle-status">
            <option value="0">未审核</option>
            <option value="1">审核</option>
        </select>
        <div class="help-block"></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? '添加' : '更新', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>